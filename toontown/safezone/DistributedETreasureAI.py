from toontown.safezone.DistributedTreasureAI import DistributedTreasureAI
from direct.directnotify.DirectNotifyGlobal import directNotify

class DistributedETreasureAI(DistributedTreasureAI):
    notify = directNotify.newCategory('DistributedETreasureAI')


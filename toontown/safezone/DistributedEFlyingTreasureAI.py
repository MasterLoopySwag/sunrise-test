from direct.directnotify.DirectNotifyGlobal import directNotify
from toontown.safezone.DistributedSZTreasureAI import DistributedSZTreasureAI

class DistributedEFlyingTreasureAI(DistributedSZTreasureAI):
    notify = directNotify.newCategory('DistributedEFlyingTreasureAI')
from toontown.classicchars.DistributedMickeyAI import DistributedMickeyAI
from direct.directnotify.DirectNotifyGlobal import directNotify

class DistributedVampireMickeyAI(DistributedMickeyAI):
    notify = directNotify.newCategory('DistributedVampireMickeyAI')


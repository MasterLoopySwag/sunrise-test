from toontown.classicchars.DistributedPlutoAI import DistributedPlutoAI
from direct.directnotify.DirectNotifyGlobal import directNotify

class DistributedWesternPlutoAI(DistributedPlutoAI):
    notify = directNotify.newCategory('DistributedWesternPlutoAI')


from toontown.classicchars.DistributedGoofySpeedwayAI import DistributedGoofySpeedwayAI
from direct.directnotify.DirectNotifyGlobal import directNotify

class DistributedSuperGoofyAI(DistributedGoofySpeedwayAI):
    notify = directNotify.newCategory('DistributedSuperGoofyAI')


from toontown.classicchars.DistributedDonaldAI import DistributedDonaldAI
from direct.directnotify.DirectNotifyGlobal import directNotify

class DistributedFrankenDonaldAI(DistributedDonaldAI):
    notify = directNotify.newCategory('DistributedFrankenDonaldAI')


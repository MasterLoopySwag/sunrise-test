from direct.distributed.DistributedObjectAI import DistributedObjectAI
from direct.directnotify.DirectNotifyGlobal import directNotify

class DistributedScavengerHuntTargetAI(DistributedObjectAI):
    notify = directNotify.newCategory('DistributedScavengerHuntTargetAI')

    def attemptScavengerHunt(self):
        pass

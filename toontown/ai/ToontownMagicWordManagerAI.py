from direct.directnotify.DirectNotifyGlobal import directNotify

from otp.ai.MagicWordManagerAI import MagicWordManagerAI
from otp.otpbase import OTPLocalizer
from otp.distributed import OtpDoGlobals

from toontown.toonbase import ToontownGlobals
from toontown.coghq import CogDisguiseGlobals
from toontown.quest import Quests
from toontown.suit import SuitDNA
from toontown.shtiker import CogPageGlobals

class ToontownMagicWordManagerAI(MagicWordManagerAI):
    notify = directNotify.newCategory('ToontownMagicWordManagerAI')

    def __init__(self, air):
        MagicWordManagerAI.__init__(self, air)

        self.air = air

    def generate(self):
        MagicWordManagerAI.generate(self)

    def announceGenerate(self):
        MagicWordManagerAI.announceGenerate(self)

    def disable(self):
        MagicWordManagerAI.disable(self)

    def delete(self):
        MagicWordManagerAI.delete(self)

    def d_setAvatarRich(self, avId, zoneId):
        if avId not in self.air.doId2do:
            return

        av = self.air.doId2do.get(avId)

        if not av:
            return

        av.b_setMoney(av.getMaxMoney())

    def d_setToonMax(self, avId, zoneId):
        if avId not in self.air.doId2do:
            return

        av = self.air.doId2do.get(avId)

        if not av:
            return
    
        av.b_setTrackAccess([1, 1, 1, 1, 1, 1, 1])
        av.b_setMaxCarry(80)
        av.experience.maxOutExp()
        av.b_setExperience(av.experience.makeNetString())
        av.inventory.zeroInv()
        av.inventory.maxOutInv(filterUberGags = 0, filterPaidGags = 0)
        av.b_setInventory(av.inventory.makeNetString()) 
        emotes = list(av.getEmoteAccess())
        for emoteId in OTPLocalizer.EmoteFuncDict.values():
           if emoteId >= len(emotes):
              continue
           emotes[emoteId] = 1
        av.b_setEmoteAccess(emotes)

        av.b_setCogParts(
            [
              CogDisguiseGlobals.PartsPerSuitBitmasks[0],
              CogDisguiseGlobals.PartsPerSuitBitmasks[1],
              CogDisguiseGlobals.PartsPerSuitBitmasks[2],
              CogDisguiseGlobals.PartsPerSuitBitmasks[3]
            ]
        )

        av.b_setCogLevels([49] * 4)
        av.b_setCogTypes([7, 7, 7, 7])

        deptCount = len(SuitDNA.suitDepts)
        av.b_setCogCount(list(CogPageGlobals.COG_QUOTAS[1]) * deptCount)
        cogStatus = [CogPageGlobals.COG_COMPLETE2] * SuitDNA.suitsPerDept
        av.b_setCogStatus(cogStatus * deptCount)
        av.b_setCogRadar([1, 1, 1, 1])
        av.b_setBuildingRadar([1, 1, 1, 1])

        numSuits = len(SuitDNA.suitHeadTypes)
        fullSetForSuit = 1 | 2 | 4
        allSummons = numSuits * [fullSetForSuit]
        av.b_setCogSummonsEarned(allSummons)

        hoods = list(ToontownGlobals.HoodsForTeleportAll)
        av.b_setHoodsVisited(hoods)
        av.b_setTeleportAccess(hoods)

        av.b_setMoney(av.getMaxMoney())
        av.b_setBankMoney(10000)

        av.b_setQuestCarryLimit(4)

        av.b_setQuests([])
        av.b_setRewardHistory(Quests.ELDER_TIER, [])

        if simbase.wantPets:
            av.b_setPetTrickPhrases(range(7))

        av.b_setTickets(99999)
        av.b_setPinkSlips(255)

        av.restockAllNPCFriends()
        av.restockAllResistanceMessages(32767)

        av.b_setMaxHp(ToontownGlobals.MaxHpLimit)
        av.toonUp(av.getMaxHp() - av.hp)

    def d_setMaxBankMoney(self, avId, zoneId):
        if avId not in self.air.doId2do:
            return

        av = self.air.doId2do.get(avId)

        av.b_setBankMoney(av.getMaxBankMoney())

    def d_setTeleportAccess(self, avId, zoneId):
        if avId not in self.air.doId2do:
            return

        av = self.air.doId2do.get(avId)   
 
        av.b_setTeleportAccess([1000, 2000, 3000, 4000, 5000, 6000, 7000, 8000, 9000, 10000, 11000, 12000, 13000])
        av.b_setHoodsVisited([1000, 2000, 3000, 4000, 5000, 6000, 7000, 8000, 9000, 10000, 11000, 12000, 13000])
        av.b_setZonesVisited([1000, 2000, 3000, 4000, 5000, 6000, 7000, 8000, 9000, 10000, 11000, 12000, 13000])

    def d_setAvatarToonUp(self, avId, zoneId):
        if avId not in self.air.doId2do:
            return

        av = self.air.doId2do.get(avId)

        av.b_setHp(av.getMaxHp())

    def d_setCogIndex(self, avId, zoneId, num):
        if avId not in self.air.doId2do:
            return

        if not -1 <= num <= 3:
            return

        av = self.air.doId2do.get(avId)

        av.b_setCogIndex(num)

    def d_setPinkSlips(self, avId, zoneId, num):
        if avId not in self.air.doId2do:
            return

        av = self.air.doId2do.get(avId)

        av.b_setPinkSlips(num)

    def d_setNewSummons(self, avId, zoneId, num):
        if avId not in self.air.doId2do:
            return

        (suitIndex, type) = num.split(' ')

        av = self.air.doId2do.get(avId)

        av.b_setCogSummonsEarned(suitIndex)
        av.addCogSummonsEarned(suitIndex, type)

    def d_restockUnites(self, avId, zoneId, num):
        if avId not in self.air.doId2do:
            return

        av = self.air.doId2do.get(avId)
        num = min(num, 32767)

        av.restockAllResistanceMessages(num)

    def d_setName(self, avId, zoneId, name):
        if avId not in self.air.doId2do:
            return

        av = self.air.doId2do.get(avId)

        av.b_setName(name)

    def d_setTickets(self, avId, zoneId, num):
        if avId not in self.air.doId2do:
            return

        av = self.air.doId2do.get(avId)

        av.b_setTickets(num)

    def d_startHoliday(self, holidayIds):
        for holidayId in holidayIds.split(','):
            cleanedId = holidayId.strip()

            if cleanedId.isdigit():
                if simbase.air.holidayManager.appendHoliday(int(cleanedId)) is False:
                    print(simbase.air.holidayManager.currentHolidays)
                    return # This holiday is already running!
                else:
                    return  # Holiday Id specified is not a valid holiday!

    def d_endHoliday(self, holidayIds):
        for holidayId in holidayIds.split(','):
            cleanedId = holidayId.strip()

            if cleanedId.isdigit():
                if simbase.air.holidayManager.removeHoliday(int(cleanedId)) is False:
                    print(simbase.air.holidayManager.currentHolidays)
                    return # This holiday has already ended!
                else:
                    return # Holiday Id specified is not a valid holiday!

    def d_sendSystemMessage(self, message, style):
        if message is '' or style is None and not isinstance(style, (int, long)):
            return

        self.air.newsManager.d_sendSystemMessage(message, style)

    def d_setCogPageFull(self, avId, zoneId, num):
        if avId not in self.air.doId2do:
            return

        av = self.air.doId2do.get(avId)

        av.b_setCogStatus([1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1])
        av.b_setCogCount([1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1])

    def d_setGhost(self, avId, zoneId):
        av = self.air.doId2do.get(avId)

        if av.ghostMode == 1:
            av.b_setGhostMode(0)
        else:
            av.b_setGhostMode(1)

    def d_spawnLBFO(self, avId, zoneId):
        av = self.air.doId2do.get(avId)

        building = av.findClosestDoor()

        if not building:
            self.notify.info('Failed to find building!')
            return # No building was found.

        self.notify.info('Found a building: {0}!'.format(building))

        building.cogdoTakeOver('l', 2, 5)
    
    def d_setGM(self, avId, zoneId, gmType):
        av = self.air.doId2do.get(avId)

        if av.isGM():
            av.b_setGM(0)

        if gmType == 1:
            av.b_setGM(1)
        elif gmType == 2:
            av.b_setGM(2)
        elif gmType == 3:
            av.b_setGM(3)
        elif gmType == 4:
            av.b_setGM(4)
    
    def d_skipVP(self, av, avId, zoneId, battle):
        from toontown.suit.DistributedSellbotBossAI import DistributedSellbotBossAI

        boss = None

        for do in simbase.air.doId2do.values():
            if isinstance(do, DistributedSellbotBossAI):
                if av.doId in do.involvedToons:
                    boss = do
                    break

        if not boss:
            self.sendResponseMessage(avId, 'You are not in a VP!')

        battle = battle.lower()

        if battle == 'three':
            if boss.state in ('PrepareBattleThree', 'BattleThree'):
                self.sendResponseMessage(avId, 'You can not return to previous rounds!')
                return
            else:
                boss.exitIntroduction()
                boss.b_setState('PrepareBattleThree')
                self.sendResponseMessage(avId, 'Skipping to final round...')
                return

        if battle == 'next':
            if boss.state in ('PrepareBattleOne', 'BattleOne'):
                boss.exitIntroduction()
                boss.b_setState('PrepareBattleThree')
                self.sendResponseMessage(avId, 'Skipping current round...')
                return

            elif boss.state in ('PrepareBattleThree', 'BattleThree'):
                boss.exitIntroduction()
                boss.b_setState('Victory')
                self.sendResponseMessage(avId, 'Skipping final round...')
                return

    def d_sendSystemMessage2(self, message):
        if message is '':
            return

        dclass = simbase.air.dclassesByName['ClientManager']
        datagram = dclass.aiFormatUpdate('systemMessage', OtpDoGlobals.OTP_DO_ID_CLIENT_MANAGER, 10, 1000000, [message])
        simbase.air.send(datagram)

    def sendResponseMessage(self, avId, message):
        invokerId = self.air.getAvatarIdFromSender()
        invoker = self.air.doId2do.get(invokerId)

        if not message or invoker:
            return

        self.sendUpdateToAvatarId(invokerId, 'setMagicWordResponse', [message])

    def checkArguments(self, args, avId):
        invokerId = self.air.getAvatarIdFromSender()

        if len(args) == 0:
            self.sendResponseMessage(invokerId, 'You specified invalid arguments for that Magic Word. Try again!')
            return False

        return True

    def setMagicWord(self, magicWord, avId, zoneId, signature):
        avId = self.air.getAvatarIdFromSender()
        av = self.air.doId2do.get(avId)

        # Chop off the ~ at the start as its not needed, split the Magic Word and make the Magic Word case insensitive.
        magicWord = magicWord[1:]
        splitWord = magicWord.split(' ')
        args = splitWord[1:]
        magicWord = splitWord[0].lower()
        del splitWord

        string = ' '.join(str(x) for x in args)
        validation = self.checkArguments(args, avId)

        if magicWord == 'rich':
            self.d_setMaxBankMoney(avId, zoneId)
        elif magicWord == 'maxbankmoney':
            self.d_setAvatarRich(avId, zoneId)
        elif magicWord == 'maxtoon':
            self.d_setToonMax(avId, zoneId)
        elif magicWord == 'toonup':
            self.d_setAvatarToonUp(avId, zoneId)
        elif magicWord == 'enabletpall':
            self.d_setTeleportAcess(avId, zoneId)
        elif magicWord == 'startholiday':
            if not validate:
                return
            self.d_startHoliday(holidayIds = args[0])
        elif magicWord == 'endholiday':
            if not validation:
                return
            self.d_endHoliday(holidayIds = args[0])
        elif magicWord == 'smsg':
            if not validation:
                return
            self.d_sendSystemMessage(message = string, style = 0) #TODO, WIP
        elif magicWord == 'cogindex':
            if not validation:
                return
            self.d_setCogIndex(avId, zoneId, num = args[0])
        elif magicWord == 'unites':
            if not validation:
                return
            self.d_restockUnites(avId, zoneId, num = args[0])
        elif magicWord == 'name':
            self.d_setName(avId, zoneId, name = string)
        elif magicWord == 'pinkslips':
            if not validation:
                return
            self.d_setPinkSlips(avId, zoneId, num = args[0])
        elif magicWord == 'tickets':
            if not validation:
                return
            self.d_setTickets(avId, zoneId, num = args[0])
        elif magicWord == 'newsummons':
            if not validation:
                return
            self.d_setNewSummons(avId, zoneId, num = args[0])
        elif magicWord == 'cogpagefull':
            if not validation:
                return
            self.d_setCogPageFull(avId, zoneId, num = args[0])
        elif magicWord == 'ghost':
            self.d_setGhost(avId, zoneId)
        elif magicWord == 'spawnlbfo':
            self.d_spawnLBFO(avId, zoneId)
        elif magicWord == 'setgm':
            if not validation:
                return
            self.d_setGM(avId, zoneId, gmType = args[0])
        elif magicWord == 'skipvp':
            if not validation:
                return
            self.d_skipVP(av, avId, zoneId, battle = string)
        elif magicWord == 'smsg2':
            if not validation:
                return
            self.d_sendSystemMessage2(message = string)
        else:
            self.notify.info('Unknown magicWord: {0} from avId: {1}!'.format(magicWord, avId))
            return

        # Call our main class:
        MagicWordManagerAI.setMagicWord(self, magicWord, avId, zoneId)
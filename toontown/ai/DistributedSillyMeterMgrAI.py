from direct.directnotify.DirectNotifyGlobal import directNotify
from direct.distributed.DistributedObjectAI import DistributedObjectAI
from toontown.ai.DistributedPhaseEventMgrAI import DistributedPhaseEventMgrAI

class DistributedSillyMeterMgrAI(DistributedPhaseEventMgrAI):
    notify = directNotify.newCategory('DistributedSillyMeterMgrAI')
    
    def __init__(self, air):
        DistributedPhaseEventMgrAI.__init__(self, air)
        
    def setCheckedPhase(self, phase):
        if phase <= -1:
           self.b_setCurPhase(-1)
           self.b_setIsRunning(False)
           return
        elif phase > 15:
           self.b_setCurPhase(-1)
           self.b_setIsRunning(False)
           return
          
        self.b_setCurPhase(phase)
        self.b_setIsRunning(True)
        messenger.send('SillyMeterPhase', [phase])
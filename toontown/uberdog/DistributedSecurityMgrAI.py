from direct.distributed.DistributedObjectGlobalAI import DistributedObjectGlobalAI
from direct.directnotify.DirectNotifyGlobal import directNotify

class DistributedSecurityMgrAI(DistributedObjectGlobalAI):
    notify = directNotify.newCategory('DistributedSecurityMgrAI')

    def requestAccountId(self, todo0, todo1, todo2):
        pass

    def requestAccountIdResponse(self, todo0, todo1):
        pass

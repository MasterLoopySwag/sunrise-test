from pandac.PandaModules import loadPrcFileData
import os

# Config override.
loadPrcFileData('', 'want-magic-words TRUE')
loadPrcFileData('', 'schellgames-dev #f')

# Get and set our playToken.
playToken = raw_input('Playtoken: ')

if not playToken:
    playToken = 'playToken'

os.environ['LOGIN_TOKEN'] = playToken

os.system('cls')

# Start the client.
from toontown.toonbase import ToontownStart

from libtoontown.pets.CPetChase import CPetChase
from libtoontown.pets.CPetBrain import CPetBrain

import __builtin__

# Set required buitlins for the UD server.
__builtin__.CPetChase = CPetChase()
__builtin__.CPetBrain = CPetBrain

# Start the UD.
from toontown.uberdog import UDStart

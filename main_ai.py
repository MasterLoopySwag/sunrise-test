from libtoontown.pets.CPetChase import CPetChase
from libtoontown.pets.CPetBrain import CPetBrain
from libtoontown.pets.CPetFlee import CPetFlee

from panda3d.toontown import DNAData, DNAStorage, DNAInteractiveProp, DNASuitPoint, DNAGroup, DNAVisGroup, SuitLeg, SuitLegList

import __builtin__

# Set required buitlins for the AI server.
__builtin__.CPetChase = CPetChase
__builtin__.CPetBrain = CPetBrain
__builtin__.CPetFlee = CPetFlee
__builtin__.DNAData = DNAData
__builtin__.DNAStorage = DNAStorage
__builtin__.DNAInteractiveProp = DNAInteractiveProp
__builtin__.DNASuitPoint = DNASuitPoint
__builtin__.DNAGroup = DNAGroup
__builtin__.DNAVisGroup = DNAVisGroup
__builtin__.SuitLeg = SuitLeg
__builtin__.SuitLegList = SuitLegList

# Start the AI.
from toontown.ai import AIStart
